var bcrypt = require('bcryptjs');
const mysql = require('mysql');
const config = require('config');
var connection = mysql.createConnection(config.get('dbConfig'));


function test () {
    console.log("I'm test function");
}

function creatUser (userdata, callback) {
  try {
      console.log('From create user function '+ userdata);
      callback(null, {msg: 'User created successfully'});
  } catch (e) {
      console.log(e);
      callback(e, null);
  }
}

async function login (data, callback) {
  var queryString = 'SELECT password, user_id from users where crn = ?';
  var a = {};
  connection.query(queryString, [data.crn], function (error, results) {
    if (error) throw error;
    if (results[0].password){
      var hash = results[0].password;
      bcrypt.compare(data.password, hash, function(err, bcryptRes) {
        if (bcryptRes === true) {
          a.isLoggedIn = true;
          a.user_id = results[0].user_id;
          a.crn = data.crn;
          console.log('User logged in successfully');
          callback(null, a);
        } else {
          callback({error: "Invalid password"}, null);
        }
      });
    } else {
      callback(error, null);
    }
  });
}

async function societyList (data, callback) {
  var queryString = 'SELECT crn, fname, mobile FROM users';
  connection.query(queryString, function (error, results) {
    if (error) callback(error, null);
    console.log(results);
    if (results){
      console.log('User Fetched');
      callback(null, results);
    }
  });
}

async function complaintList (data, callback) {
  var queryString = 'SELECT * FROM complaints';
  connection.query(queryString, function (error, results) {
    if (error) callback(error, null);
    console.log(results);
    if (results){
      console.log('Complaints Fetched');
      callback(null, results);
    }
  });
}

async function registerComplaint (data, callback) {
  var flag = false;
  var queryString = 'SELECT COUNT(*) as count from users where crn = ? and email = ?';
  connection.query(queryString, [crn, data.email], function (error, results, fields) {
    if (error) throw error;
    if (results[0].count == 1){
      flag = true;
      console.log('Flag set to true');
      var queryString1 = 'INSERT INTO complaints (crn, doc, user_id, description, incident_location, outcome) values (?, ?, ?, ?, ?, ?)';
      return connection.query(queryString1, [crn, data.date, user_id, data.description, data.location, data.outcome], function (error, results, fields) {
        if (error) throw error;
        if (results.affectedRows === 1){
          console.log('Complaint posted successfully');
          callback(null, {
            'message': 'Complaint registered successfully'
          })
        }
      });
    }
  });
}

const indexFunctions = {
    'test': test,
    'creatUser': creatUser,
    'login': login,
    'societyList': societyList,
    'complaintList': complaintList,
    'registerComplaint': registerComplaint
}

module.exports = indexFunctions;