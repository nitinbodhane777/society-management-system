const express = require('express');
const router = express.Router();
const serviceHelper = require('../services/index');
const mysql = require('mysql');
const config = require('config');
var validator = require('validator');
var bcrypt = require('bcryptjs');
const multer = require('multer');
var upload = multer({'dest': './uploads'});
const connection = mysql.createConnection(config.get('dbConfig'));
isLoggedIn = false;
user_id = null;
crn = null;



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('ejs/home', {"isLoggedIn": isLoggedIn});
});

router.get('/login', function(req, res, next) {
  res.render('ejs/login');
});

router.get('/about', function(req, res, next) {
  res.render('ejs/about', {"isLoggedIn": isLoggedIn});
});

router.get('/contact', function(req, res, next) {
  res.render('ejs/contact_us', {"isLoggedIn": isLoggedIn});
});
router.get('/complaint', function(req, res, next){
  res.render('ejs/complaint');
});

router.get('/electricity', function(req, res, next) {
  res.render('ejs/electricity');
});
router.get('/maintainance', function(req, res, next){
  res.render('ejs/maintainance');
});

router.get('/register', function(req, res, next) {
  res.render('ejs/register', {"isLoggedIn": isLoggedIn});
});

router.get('/payment', function(req, res, next) {
  res.render('ejs/payment');
});

router.get('/forgot', function(req, res, next){
  res.render('ejs/forgotPassword');
});

router.get('/logout', function(req, res, next) {
  isLoggedIn = false;
  res.redirect('/');
});

router.get('/all_complaints', function(req, res, next) {
  var data = {};
  data.isLoggedIn = isLoggedIn;
  serviceHelper.complaintList(data, async (err, data) => {
    if (err) {
      console.log(err);
      throw err;
    } else {
      console.log('Success');
      res.render('ejs/all_complaints', {'data': data});
    }
  });
});

router.get('/society-details', (req, res, next) => {
  var data = {};
  data.isLoggedIn = isLoggedIn;
  serviceHelper.societyList(data, async (err, data) => {
    if (err) {
      console.log(err);
      throw err;
    } else {
      console.log('Success');
      res.render('ejs/society_data', {'data': data});
    }
  });
});

router.post('/register-complaint', function(req, res, next) {
  var data = req.body;
  serviceHelper.registerComplaint(data, async (err, data) => {
    if (err) {
      console.log(err);
      res.redirect('/complaint');
    } else {
      console.log('Success');
      res.redirect('/');
    }
  });
});

router.post('/user-login', (req, res, next) => {
  var data = req.body;
  var usersData = serviceHelper.login(data, async (err, data) => {
    if (err) {
      console.log(err);
      res.redirect('/login');
    } else {
      isLoggedIn = data.isLoggedIn;
      crn = data.crn;
      user_id = data.user_id;
      res.redirect('/');
    }
  });
});

router.post('/user-registration', upload.single('profileImage'),  function(req, res, next) {
  var data = req.body;
  //  Form validation
  // console.log(validator.isEmpty(data.fname, [{ignore_whitespace:true}]));
  if (req.file) {
    console.log('Image Uploaded successfully');
  } else {
    console.log('Image failed to uploaded');
  }
  // var connection = mysql.createConnection(config.get('dbConfig'));
  connection.connect();
  var queryString = 'Update users set fname = ?, lname = ?, email = ?, mobile = ?, password = ? where crn = ?';
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(data.password, salt, function(err, hash) {
        // Store hash in your password DB.
        console.log('hash1 - ', hash);
        data.password = hash;
        connection.query(queryString, [data.fname, data.lname, data.email, data.num, data.password, data.crn], function (error, results, fields) {
          if (error) throw error;
          if (results.affectedRows === 1){
            console.log('User Created and logged in successfully');
            isLoggedIn = true;
            crn = data.crn;
            res.redirect('/');
          } else {
            isLoggedIn = false;
            res.redirect('/register');
          }
        });
        connection.end();
    });
  });
  // var usersData = serviceHelper.createUser(data, function (err, data){
  //   console.log(data);
  //   if (err) {
  //     console.log(err);
  //   }
  // });
});

module.exports = router;
